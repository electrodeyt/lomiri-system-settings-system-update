# Czech translation for lomiri-system-settings-system-update
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-31 11:30+0000\n"
"PO-Revision-Date: 2023-04-02 03:46+0000\n"
"Last-Translator: Jozef Mlich <jmlich83@gmail.com>\n"
"Language-Team: Czech <https://hosted.weblate.org/projects/lomiri/"
"lomiri-system-settings-system-update/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 4.17-dev\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#: plugins/system-update/UpdateSettings.qml:33
#: plugins/system-update/PageComponent.qml:44
msgid "Update settings"
msgstr "Nastavení aktualizací"

#: plugins/system-update/UpdateSettings.qml:56
#: plugins/system-update/Configuration.qml:30
msgid "Auto download"
msgstr "Automatické stahování"

#: plugins/system-update/UpdateSettings.qml:59
#: plugins/system-update/Configuration.qml:54
msgid "Never"
msgstr "Nikdy"

#: plugins/system-update/UpdateSettings.qml:61
msgid "On Wi-Fi"
msgstr "Přes Wi-Fi"

#: plugins/system-update/UpdateSettings.qml:63
msgid "Always"
msgstr "Vždy"

#: plugins/system-update/UpdateSettings.qml:65
msgid "Unknown"
msgstr "Neznámo"

#: plugins/system-update/UpdateSettings.qml:71
msgid "Channels"
msgstr "Kanály"

#: plugins/system-update/UpdateSettings.qml:89
#: plugins/system-update/ReinstallAllApps.qml:37
#: plugins/system-update/ReinstallAllApps.qml:96
msgid "Reinstall all apps"
msgstr "Přeinstalovat všechny aplikace"

#: plugins/system-update/InstallationFailed.qml:26
#: plugins/system-update/DownloadHandler.qml:175
msgid "Installation failed"
msgstr "Instalace se nezdařila"

#: plugins/system-update/InstallationFailed.qml:29
msgid "OK"
msgstr "OK"

#: plugins/system-update/ReinstallAllApps.qml:86
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""
"Použijte toto pro získání všech nejnovějších aplikací, které jsou obvykle "
"potřebné po přechodu na novější hlavní verzi systému, "

#: plugins/system-update/ReinstallAllApps.qml:150
#: plugins/system-update/PageComponent.qml:223
msgid "Connect to the Internet to check for updates."
msgstr "Připojit k Internetu a zkontrolovat aktualizace."

#: plugins/system-update/ReinstallAllApps.qml:152
#: plugins/system-update/PageComponent.qml:225
msgid "Software is up to date"
msgstr "Software je aktuální"

#: plugins/system-update/ReinstallAllApps.qml:155
#: plugins/system-update/PageComponent.qml:228
msgid "The update server is not responding. Try again later."
msgstr "Server aktualizací neodpovídá. Zkuste to později."

#: plugins/system-update/ReinstallAllApps.qml:164
#: plugins/system-update/PageComponent.qml:237
msgid "Updates Available"
msgstr "Dostupné aktualizace"

#: plugins/system-update/ChannelSettings.qml:34
msgid "Channel settings"
msgstr "Nastavení kanálu"

#: plugins/system-update/ChannelSettings.qml:107
msgid "Fetching channels"
msgstr "Načítání kanálů"

#: plugins/system-update/ChannelSettings.qml:116
msgid "Channel to get updates from:"
msgstr "Kanál pro získávání aktualizací:"

#: plugins/system-update/ChannelSettings.qml:132
msgid "Switching channel"
msgstr "Přepínání kanálu"

#: plugins/system-update/UpdateDelegate.qml:120
msgid "Retry"
msgstr "Zkusit znovu"

#: plugins/system-update/UpdateDelegate.qml:125
msgid "Update"
msgstr "Aktualizovat"

#: plugins/system-update/UpdateDelegate.qml:127
msgid "Download"
msgstr "Stáhnout"

#: plugins/system-update/UpdateDelegate.qml:133
msgid "Resume"
msgstr "Pokračovat"

#: plugins/system-update/UpdateDelegate.qml:140
msgid "Pause"
msgstr "Pozastavit"

#: plugins/system-update/UpdateDelegate.qml:144
msgid "Install…"
msgstr "Nainstalovat…"

#: plugins/system-update/UpdateDelegate.qml:146
msgid "Install"
msgstr "Nainstalujte"

#: plugins/system-update/UpdateDelegate.qml:150
msgid "Open"
msgstr "Otevřít"

#: plugins/system-update/UpdateDelegate.qml:229
msgid "Installing"
msgstr "Instalace"

#: plugins/system-update/UpdateDelegate.qml:233
msgid "Paused"
msgstr "Pozastaveno"

#: plugins/system-update/UpdateDelegate.qml:236
msgid "Waiting to download"
msgstr "Čeká na stažení"

#: plugins/system-update/UpdateDelegate.qml:239
msgid "Downloading"
msgstr "Stahování"

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: plugins/system-update/UpdateDelegate.qml:275
#, qt-format
msgid "%1 of %2"
msgstr "%1 z %2"

#: plugins/system-update/UpdateDelegate.qml:279
msgid "Downloaded"
msgstr "Staženo"

#: plugins/system-update/UpdateDelegate.qml:282
msgid "Installed"
msgstr "Nainstalováno"

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: plugins/system-update/UpdateDelegate.qml:287
#, qt-format
msgid "Updated %1"
msgstr "Aktualizováno %1"

#: plugins/system-update/UpdateDelegate.qml:311
msgid "Update failed"
msgstr "Aktualizace selhala"

#: plugins/system-update/ImageUpdatePrompt.qml:31
msgid "Update System"
msgstr "Aktualizovat systém"

#: plugins/system-update/ImageUpdatePrompt.qml:33
msgid "The device needs to restart to install the system update."
msgstr "Pro instalaci systémových aktualizací musí být přístroj restartován."

#: plugins/system-update/ImageUpdatePrompt.qml:34
msgid "Connect the device to power before installing the system update."
msgstr "Před instalací systémové aktualizace připojte zařízení k nabíječce."

#: plugins/system-update/ImageUpdatePrompt.qml:39
msgid "Restart & Install"
msgstr "Restartovat a nainstalovat"

#: plugins/system-update/ImageUpdatePrompt.qml:49
#: plugins/system-update/PageComponent.qml:125
msgid "Cancel"
msgstr "Zrušit"

#: plugins/system-update/PageComponent.qml:38
msgid "Updates"
msgstr "Aktualizace"

#: plugins/system-update/PageComponent.qml:52
#: plugins/system-update/PageComponent.qml:111
msgid "Clear updates"
msgstr "Vyčistit aktualizace"

#: plugins/system-update/PageComponent.qml:112
msgid "Clear the update list?"
msgstr "Vyčistit seznam aktualizací?"

#: plugins/system-update/PageComponent.qml:129
msgid "Clear"
msgstr "Vyčistit"

#: plugins/system-update/PageComponent.qml:371
msgid "Recent updates"
msgstr "Nedávné aktualizace"

#: plugins/system-update/GlobalUpdateControls.qml:85
msgid "Checking for updates…"
msgstr "Zjišťování dostupnosti aktualizací…"

#: plugins/system-update/GlobalUpdateControls.qml:90
msgid "Stop"
msgstr "Zastavit"

#. TRANSLATORS: %1 is number of software updates available.
#: plugins/system-update/GlobalUpdateControls.qml:117
#, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "%1 dostupná aktualizace"
msgstr[1] "%1 dostupné aktualizace"
msgstr[2] "%1 dostupných aktualizací"

#: plugins/system-update/GlobalUpdateControls.qml:127
msgid "Update all…"
msgstr "Aktualizovat vše…"

#: plugins/system-update/GlobalUpdateControls.qml:129
msgid "Update all"
msgstr "Aktualizovat vše"

#: plugins/system-update/FirmwareUpdate.qml:37
#: plugins/system-update/FirmwareUpdate.qml:158
msgid "Firmware Update"
msgstr "Aktualizace firmware"

#: plugins/system-update/FirmwareUpdate.qml:134
msgid "There is a firmware update available!"
msgstr "Je k dispozici aktualizace firmware!"

#: plugins/system-update/FirmwareUpdate.qml:135
msgid "Firmware is up to date!"
msgstr "Firmware je aktuální!"

#: plugins/system-update/FirmwareUpdate.qml:170
msgid "The device will restart automatically after installing is done."
msgstr "Po dokončení instalace bude zařízení automaticky restartováno."

#: plugins/system-update/FirmwareUpdate.qml:186
msgid "Install and restart now"
msgstr "Nainstalovat a restartovat nyní"

#: plugins/system-update/FirmwareUpdate.qml:229
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""
"Stahují se a instalují aktualizace firmware – může to trvat několik minut…"

#: plugins/system-update/FirmwareUpdate.qml:236
msgid "Checking for firmware update"
msgstr "Zjišťování dostupnosti aktualizace firmware"

#: plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr "Stahovat budoucí aktualizace automaticky:"

#: plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr "Přes Wi-Fi"

#: plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr "Přes kterékoliv datové spojení"

#: plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr "Přenos dat může být zpoplatněn."

#: plugins/system-update/ChangelogExpander.qml:43
#, qt-format
msgid "Version %1"
msgstr "Verze %1"

#~ msgid "Development"
#~ msgstr "Vývojový"

#~ msgid "Release candidate"
#~ msgstr "Kandidát na vydání"

#~ msgid "Stable"
#~ msgstr "Stabilní"

#~ msgid "system"
#~ msgstr "systém"

#~ msgid "software"
#~ msgstr "software"

#~ msgid "update"
#~ msgstr "aktualizace"

#~ msgid "apps"
#~ msgstr "aplikace"

#~ msgid "application"
#~ msgstr "aplikace"

#~ msgid "automatic"
#~ msgstr "automaticky"

#~ msgid "download"
#~ msgstr "stáhnout"

#~ msgid "upgrade"
#~ msgstr "přechod na novější verzi"

#~ msgid "click"
#~ msgstr "klik"

#, fuzzy
#~| msgid "Updates Available"
#~ msgid "Updates available"
#~ msgstr "Dostupné aktualizace"
