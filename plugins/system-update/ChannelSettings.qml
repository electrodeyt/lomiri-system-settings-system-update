/*
 * This file is part of system-settings
 *
 * Copyright (C) 2017 The UBports project
 * Copyright (C) 2020 UBports Foundation
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Marius Gripsgard <marius@ubports.com>
 */

import QtQuick 2.4
import SystemSettings 1.0
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import Lomiri.Connectivity 1.0
import Lomiri.SystemSettings.Update 1.0
import "i18nd.js" as I18nd
import "ChUtils.js" as ChUtils

ItemPage {
    id: root
    objectName: "channelSettingsPage"
    title: I18nd.tr("Channel settings")

    signal channelChanged()

    Connections {
      target: UpdateManager
      onStatusChanged: {
        switch (UpdateManager.status) {
        case UpdateManager.StatusCheckingImageUpdates:
        case UpdateManager.StatusCheckingAllUpdates:
            aIndicator.visible = true;
            channelSelector.visible = false;
            return;
        }
        aIndicator.visible = false;
        channelSelector.visible = true;

        if (channelSelectorModel.count !== 0)
          return;
        appendChannels()
        return;
      }
    }

    function appendChannels() {
      /* FIXME: this code relies on channels being named in a certain way.
       * TODO: it would be nice to have a labeled divider for each series.
       */

      if (channelSelectorModel.count !== 0)
        return;

      let current = ChUtils.parseChannel(SystemImage.getSwitchChannel());

      SystemImage.getChannels().forEach(function (_channel) {
          let channel = ChUtils.parseChannel(_channel)

          /* Try to prevent series downgrading from the UI - still possible via CLI/installer.
           * Because series value are in a certain format, we can rely on lexical ordering here */
          if (channel.series < current.series)
            return;

          channelSelectorModel.append({ name: channel.prettyName, description: "", channel: _channel});
      });
      setSelectedChannel();
    }

    function setSelectedChannel() {
      var selNum = -1;
      var channel = SystemImage.getSwitchChannel();
      for (var i = 0; i < channelSelectorModel.count; i++) {
        if (channelSelectorModel.get(i).channel === channel){
            selNum = i;
            break;
        }
      }
      channelSelector.selectedIndex = selNum;
      return selNum;
    }

    Column {
        id: aIndicator
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        visible: false

        ActivityIndicator {
            opacity: visible ? 1 : 0
            visible: parent.visible
            running: visible
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            id: aIndicatorText
            text: I18nd.tr("Fetching channels")
            visible: parent.visible
        }
    }

    ListItem.ItemSelector {
        id: channelSelector
        anchors.top: root.header.bottom
        expanded: true
        text: I18nd.tr ("Channel to get updates from:")
        model: channelSelectorModel
        delegate: selectorDelegate

        onDelegateClicked: {
            // Cancel all ongoing updates before switching
            SystemImage.cancelUpdate()
            const channel = channelSelectorModel.get(index).channel
            SystemImage.setSwitchChannel(channel);

            if (channel === SystemImage.channelName) {
                SystemImage.setSwitchBuild(SystemImage.currentBuildNumber);
            } else {
                SystemImage.setSwitchBuild(0);
            }
            UpdateManager.check(UpdateManager.CheckImage);
            aIndicatorText.text = I18nd.tr("Switching channel");

            root.channelChanged()
        }
    }

    Component {
        id: selectorDelegate
        OptionSelectorDelegate { text: name; subText: description; }
    }

    ListModel {
        id: channelSelectorModel
        Component.onCompleted: {
          switch (UpdateManager.status) {
          case UpdateManager.StatusCheckingImageUpdates:
          case UpdateManager.StatusCheckingAllUpdates:
              aIndicator.visible = true;
              channelSelector.visible = false;
              return;
          }
          if (SystemImage.getChannels().length !== 0){
            appendChannels()
            return;
          }
          UpdateManager.check(UpdateManager.CheckImage);
          return;
        }
    }
}
